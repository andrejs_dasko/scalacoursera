# Completed assignments for Scala courses on Coursera

This repository contains solved assignments of the following Coursera courses:	
- Functional Programming Principles in Scala [link](https://www.coursera.org/learn/progfun1)	
- Functional Program Design in Scala [link](https://www.coursera.org/learn/progfun2)	
- Parallel programming [link](https://www.coursera.org/learn/parprog1)	
Please find completed assignments in the [assignments folder](assignments).