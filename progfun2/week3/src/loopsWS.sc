object loopsWS {
  println("Welcome to the Scala worksheet")
  
  def REPEAT(condition: => Boolean)(command: => Unit): Unit = {
  	command
  	if (!condition) {
  		REPEAT(condition)(command)
  	} else {}
  }
  
  var i = 0;
  REPEAT(i > 5)({i == i + 1; println(i)})
}