package week1

object generatorsWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  trait Generator[+T] {
  	self =>
  
  	def generate: T
  	
  	def map[S](f: T => S): Generator[S] = new Generator[S] {
  		def generate: S = f(self.generate)
  	}
  	
  	def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
  		def generate: S = f(self.generate).generate
  	}
  }
  
  def single[T](x: T) = new Generator[T] {
  	def generate: T = x
  }                                               //> single: [T](x: T)week1.generatorsWS.Generator[T]
  
  val integers = new Generator[Int] {
  	def generate = scala.util.Random.nextInt()
  }                                               //> integers  : week1.generatorsWS.Generator[Int] = week1.generatorsWS$$anonfun$
                                                  //| main$1$$anon$4@3b6eb2ec
  
	val booleans = for (x <- integers) yield x > 0
                                                  //> booleans  : week1.generatorsWS.Generator[Boolean] = week1.generatorsWS$$anon
                                                  //| fun$main$1$Generator$1$$anon$1@3f3afe78
                   
  val randomInt = integers.generate               //> randomInt  : Int = 1987517230

	val pairs = for {
		x <- integers
		y <- integers
	} yield (x, y)                            //> pairs  : week1.generatorsWS.Generator[(Int, Int)] = week1.generatorsWS$$anon
                                                  //| fun$main$1$Generator$1$$anon$2@5b464ce8
	            
	pairs.generate                            //> res0: (Int, Int) = (656619196,-1679381213)
  
  def lists: Generator[List[Int]] = for {
  	isEmpty <- booleans
  	list <- if (isEmpty) emptyLists else nonEmptyLists
  } yield list                                    //> lists: => week1.generatorsWS.Generator[List[Int]]
  
  def emptyLists = single(Nil)                    //> emptyLists: => week1.generatorsWS.Generator[scala.collection.immutable.Nil.t
                                                  //| ype]
  
  def nonEmptyLists = for {
  	head <- integers
  	tail <- lists
  } yield head :: tail                            //> nonEmptyLists: => week1.generatorsWS.Generator[List[Int]]
  
  lists.generate                                  //> res1: List[Int] = List(1962361097)
  
  trait Tree
  case class Inner(left: Tree, right: Tree) extends Tree
  case class Leaf(x: Int) extends Tree
  
  def trees: Generator[Tree] = for {
  	isLeaf <- booleans
  	tree <- if (isLeaf) leefs else inners
  } yield tree                                    //> trees: => week1.generatorsWS.Generator[week1.generatorsWS.Tree]

  def leefs: Generator[Leaf] = for (x <- integers) yield new Leaf(x)
                                                  //> leefs: => week1.generatorsWS.Generator[week1.generatorsWS.Leaf]

  def inners: Generator[Inner] = for {
  	left <- trees
  	right <- trees
  } yield new Inner(left, right)                  //> inners: => week1.generatorsWS.Generator[week1.generatorsWS.Inner]
  
  trees.generate                                  //> res2: week1.generatorsWS.Tree = Inner(Inner(Inner(Leaf(-1412510575),Leaf(-7
                                                  //| 49265102)),Inner(Leaf(-1096292852),Leaf(856031704))),Leaf(116976740))
 
}