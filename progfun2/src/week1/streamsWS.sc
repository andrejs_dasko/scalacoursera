package week1

object streamsWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def from(n: Int): Stream[Int] = n #:: from(n + 1)
                                                  //> from: (n: Int)Stream[Int]
  
  val nats = from(0)                              //> nats  : Stream[Int] = Stream(0, ?)
  
  nats.take(4).foreach(println)                   //> 0
                                                  //| 1
                                                  //| 2
                                                  //| 3
  nats.take(4).toList                             //> res0: List[Int] = List(0, 1, 2, 3)
  val m4 = nats.map(_ * 4)                        //> m4  : scala.collection.immutable.Stream[Int] = Stream(0, ?)
  
  m4.take(4).foreach(println)                     //> 0
                                                  //| 4
                                                  //| 8
                                                  //| 12
  m4.take(4).toList                               //> res1: List[Int] = List(0, 4, 8, 12)
  
  def sieve(s: Stream[Int]): Stream[Int] =
  	s.head #:: sieve(s.tail filter (_ % s.head != 0))
                                                  //> sieve: (s: Stream[Int])Stream[Int]
                                                  
	val primes = sieve(from(2))               //> primes  : Stream[Int] = Stream(2, ?)
	
	primes.take(40).toList                    //> res2: List[Int] = List(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 4
                                                  //| 7, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131
                                                  //| , 137, 139, 149, 151, 157, 163, 167, 173)
}