package week1

object booksWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  val f: String => String = { case "ping" => "pong" }
                                                  //> f  : String => String = <function1>
  f("ping")                                       //> res0: String = pong
  
  case class Book(title: String, authors: List[String])
  
  val books: Set[Book] = Set (
    Book(title = "Structure and Interpretation of Computer Programs",
      authors = List("Abelson, Harald", "Sussman, Gerald J.")),
    Book(title = "Introduction to Functional Programming",
      authors = List("Bird, Richard", "Wadler, Phil", "Bird, Paul")),
    Book(title = "Effective Java",
      authors = List("Bloch, Joshua")),
    Book(title = "Java Puzzlers",
      authors = List("Bloch, Joshua", "Gafter, Neal")),
    Book(title = "Programming in Scala",
      authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill")))
                                                  //> books  : Set[week1.booksWS.Book] = Set(Book(Introduction to Functional Progr
                                                  //| amming,List(Bird, Richard, Wadler, Phil, Bird, Paul)), Book(Programming in S
                                                  //| cala,List(Odersky, Martin, Spoon, Lex, Venners, Bill)), Book(Structure and I
                                                  //| nterpretation of Computer Programs,List(Abelson, Harald, Sussman, Gerald J.)
                                                  //| ), Book(Effective Java,List(Bloch, Joshua)), Book(Java Puzzlers,List(Bloch, 
                                                  //| Joshua, Gafter, Neal)))
  
  for (b <- books; a <- b.authors; if a startsWith "Bird")
  yield b.title                                   //> res1: scala.collection.immutable.Set[String] = Set(Introduction to Functiona
                                                  //| l Programming)
  
  books.flatMap( b => for(a <- b.authors; if a startsWith "Bird") yield b.title )
                                                  //> res2: scala.collection.immutable.Set[String] = Set(Introduction to Functiona
                                                  //| l Programming)
  books.flatMap( b => for(a <- b.authors.withFilter(a => a startsWith "Bird")) yield b.title )
                                                  //> res3: scala.collection.immutable.Set[String] = Set(Introduction to Function
                                                  //| al Programming)
  books.flatMap(b => b.authors.withFilter(a => a startsWith "Bird") map (_ => b.title))
                                                  //> res4: scala.collection.immutable.Set[String] = Set(Introduction to Function
                                                  //| al Programming)
  
  books.flatMap(book => book.authors.withFilter(author => author startsWith "Bird") map (_ => book.title))
                                                  //> res5: scala.collection.immutable.Set[String] = Set(Introduction to Function
                                                  //| al Programming)
}