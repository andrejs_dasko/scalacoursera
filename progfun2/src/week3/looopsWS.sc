package week3

object looopsWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def WHILE(condition: => Boolean)(command: => Unit): Unit = {
  	if (condition) {
  		command
  		WHILE(condition)(command)
  	} else ()
  }                                               //> WHILE: (condition: => Boolean)(command: => Unit)Unit
  
  def REPEAT(command: => Unit)(condition: => Boolean): Unit = {
  	command
  	if (condition) () else REPEAT(command)(condition)
  }                                               //> REPEAT: (command: => Unit)(condition: => Boolean)Unit
  
  var i = 0                                       //> i  : Int = 0
  WHILE(i < 3)({println(i);i = i + 1})            //> 0
                                                  //| 1
                                                  //| 2
  REPEAT({println(i);i = i + 1})(i > 3)           //> 3
	for (i <- 1 until 3) println(i)           //> 1
                                                  //| 2
}