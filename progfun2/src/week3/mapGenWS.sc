package week3

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

object mapGenWS {
  println("Welcome to the Scala worksheet")
  
  lazy val genMap: Gen[Map[Int,Int]] = for {
	  k <- arbitrary[Int]
	  v <- arbitrary[Int]
	  m <- oneOf(const(Map.empty[Int,Int]), genMap)
} yield m.updated(k, v)
}