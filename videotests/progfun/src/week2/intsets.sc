package week2

object intsets {
  val t1 = new NonEmpty(5, Empty, Empty)          //> t1  : week2.NonEmpty = {.5.}
  val t2 = t1 include 4                           //> t2  : week2.IntSet = {{.4.}5.}
  val t3 = new NonEmpty(6, Empty, Empty)          //> t3  : week2.NonEmpty = {.6.}
  val t23 = t2 union t3                           //> t23  : week2.IntSet = {{.4{.5.}}6.}
}

abstract class IntSet {
	def contains(x: Int): Boolean
	def include(x: Int): IntSet
	def union(other: IntSet): IntSet
}

class Empty extends IntSet {
	def contains(x: Int) = false
	def include(x: Int): IntSet = new NonEmpty(x, Empty, Empty)
	def union(other: IntSet): IntSet = other
	override def toString = "."
}

class NonEmpty(num: Int, left: IntSet, right: IntSet) extends IntSet {
	def contains(x: Int): Boolean = {
		if (x < num) left contains x
		else if (x > num) right contains x
		else true
	}
	def include(x: Int): IntSet = {
		if (x < num) new NonEmpty(num, left include x, right)
		else if (x > num) new NonEmpty(num, left, right include x)
		else this
	}
	def union(other: IntSet): IntSet = ((left union right) union other) include num
	
	override def toString = "{" + left + num + right + "}"
}

// objects in scala are values, so no evaluation occurs upon usage
object Empty extends IntSet {
	def contains(x: Int) = false
	def include(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
	override def toString = "."
	def union(other: IntSet): IntSet = other
}