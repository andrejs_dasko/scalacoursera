object v7FactorialWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def factorial(n: Int): Int = {
  	if (n == 0) 1 else n * factorial(n - 1)
  }                                               //> factorial: (n: Int)Int
  
  factorial(1)                                    //> res0: Int = 1
  factorial(3)                                    //> res1: Int = 6
  factorial(4)                                    //> res2: Int = 24
  
  def factorialTail(n: Int): Int = {
  	
  	def loop(n: Int, acc: Int): Int =
  		if (n == 0) acc else loop(n- 1, acc * n)
  	
  	loop(n, 1)
  }                                               //> factorialTail: (n: Int)Int
  
  factorialTail(1)                                //> res3: Int = 1
  factorialTail(2)                                //> res4: Int = 2
  factorialTail(3)                                //> res5: Int = 6
  factorialTail(4)                                //> res6: Int = 24
  
  def fact(x: Int): Int = if (x == 0) 1 else fact(x - 1)
                                                  //> fact: (x: Int)Int
  
  fact(1)                                         //> res7: Int = 1
  fact(2)                                         //> res8: Int = 1
  fact(3)                                         //> res9: Int = 1
  fact(4)                                         //> res10: Int = 1
}