package v6BlocksAndLexicalScope

object NestedFunctions {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  def abs(x: Double) = if (x < 0) -x else x       //> abs: (x: Double)Double

  def sqr(x: Double): Double = x * x              //> sqr: (x: Double)Double

  def sqrt(x: Double) = {
  
    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    def isGoodEnough(guess: Double) =
      abs(sqr(guess) - x) / x < 0.00001

    def improve(guess: Double) = (guess + x / guess) / 2
    
    sqrtIter(1.0)
  }                                               //> sqrt: (x: Double)Double
 	sqrt(1)                                   //> res0: Double = 1.0
	sqrt(2)                                   //> res1: Double = 1.4142156862745097
	sqrt(4)                                   //> res2: Double = 2.0000000929222947
	sqrt(1e60)                                //> res3: Double = 1.0000000031080746E30
	sqrt(1e-6)                                //> res4: Double = 0.0010000001533016628
}