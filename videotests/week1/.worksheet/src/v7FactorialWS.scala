object v7FactorialWS {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(66); 
  println("Welcome to the Scala worksheet");$skip(83); 
  
  def factorial(n: Int): Int = {
  	if (n == 0) 1 else n * factorial(n - 1)
  };System.out.println("""factorial: (n: Int)Int""");$skip(18); val res$0 = 
  
  factorial(1);System.out.println("""res0: Int = """ + $show(res$0));$skip(15); val res$1 = 
  factorial(3);System.out.println("""res1: Int = """ + $show(res$1));$skip(15); val res$2 = 
  factorial(4);System.out.println("""res2: Int = """ + $show(res$2));$skip(148); 
  
  def factorialTail(n: Int): Int = {
  	
  	def loop(n: Int, acc: Int): Int =
  		if (n == 0) acc else loop(n- 1, acc * n)
  	
  	loop(n, 1)
  };System.out.println("""factorialTail: (n: Int)Int""");$skip(22); val res$3 = 
  
  factorialTail(1);System.out.println("""res3: Int = """ + $show(res$3));$skip(19); val res$4 = 
  factorialTail(2);System.out.println("""res4: Int = """ + $show(res$4));$skip(19); val res$5 = 
  factorialTail(3);System.out.println("""res5: Int = """ + $show(res$5));$skip(19); val res$6 = 
  factorialTail(4);System.out.println("""res6: Int = """ + $show(res$6));$skip(60); 
  
  def fact(x: Int): Int = if (x == 0) 1 else fact(x - 1);System.out.println("""fact: (x: Int)Int""");$skip(13); val res$7 = 
  
  fact(1);System.out.println("""res7: Int = """ + $show(res$7));$skip(10); val res$8 = 
  fact(2);System.out.println("""res8: Int = """ + $show(res$8));$skip(10); val res$9 = 
  fact(3);System.out.println("""res9: Int = """ + $show(res$9));$skip(10); val res$10 = 
  fact(4);System.out.println("""res10: Int = """ + $show(res$10))}
}
