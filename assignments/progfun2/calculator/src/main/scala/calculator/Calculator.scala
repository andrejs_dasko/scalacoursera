package calculator

sealed abstract class Expr
final case class Literal(v: Double) extends Expr
final case class Ref(name: String) extends Expr
final case class Plus(a: Expr, b: Expr) extends Expr
final case class Minus(a: Expr, b: Expr) extends Expr
final case class Times(a: Expr, b: Expr) extends Expr
final case class Divide(a: Expr, b: Expr) extends Expr

object Calculator {
  def computeValues(
      namedExpressions: Map[String, Signal[Expr]]): Map[String, Signal[Double]] = {
    namedExpressions.map(pair => (pair._1, Signal(eval(pair._2(), namedExpressions))))
  }

  def eval(expr: Expr, references: Map[String, Signal[Expr]]): Double = {
    expr match {
      case Literal(v) => v
      
      case Ref(n) => eval(getReferenceExpr(n, references), references - n)
      
      case Plus(Literal(v), Literal(v2)) => v + v2
      case Plus(Literal(v), Ref(n)) => v + eval(Ref(n), references)
      case Plus(Ref(n), Literal(v)) => eval(Plus(Literal(v), Ref(n)), references)
      case Plus(Ref(n), Ref(m)) => eval(Ref(n), references) + eval(Ref(m), references)

      case Minus(Literal(v), Literal(v2)) => v - v2
      case Minus(Literal(v), Ref(n)) => v - eval(Ref(n), references)
      case Minus(Ref(n), Literal(v)) => eval(Ref(n), references) - v
      case Minus(Ref(n), Ref(m)) => eval(Ref(n), references) - eval(Ref(m), references)

      case Times(Literal(v), Literal(v2)) => v * v2
      case Times(Literal(v), Ref(n)) => v * eval(Ref(n), references)
      case Times(Ref(n), Literal(v)) => eval(Times(Literal(v), Ref(n)), references)
      case Times(Ref(n), Ref(m)) => eval(Ref(n), references) * eval(Ref(m), references)

      case Divide(Literal(v), Literal(v2)) => v / v2
      case Divide(Literal(v), Ref(n)) => v / eval(Ref(n), references)
      case Divide(Ref(n), Literal(v)) => eval(Ref(n), references) / v
      case Divide(Ref(n), Ref(m)) => eval(Ref(n), references) / eval(Ref(m), references)
      
      case _ => Double.NaN
    }
  }

  /** Get the Expr for a referenced variables.
   *  If the variable is not known, returns a literal NaN.
   */
  private def getReferenceExpr(name: String,
      references: Map[String, Signal[Expr]]): Expr = {
    references.get(name).fold[Expr] {
      Literal(Double.NaN)
    } { exprSignal =>
      exprSignal()
    }
  }
}
