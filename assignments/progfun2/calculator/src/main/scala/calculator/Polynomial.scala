package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal(b() * b() - 4 * a() * c())
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    def compute: Set[Double] = {
      def findRoot(f: (Double, Double) => Double): Double = f(b() * -1, Math.sqrt(delta())) / (2 * a())
      if (delta() < 0) Set()
      else {
        Set(findRoot((x, y) => x + y), findRoot((x, y) => x - y))
      }
    }
    Signal(compute)
  }
}
