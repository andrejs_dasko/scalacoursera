package calculator

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import org.scalatest._

import TweetLength.MaxTweetLength

@RunWith(classOf[JUnitRunner])
class CalculatorSuite extends FunSuite with ShouldMatchers {

  /******************
   ** TWEET LENGTH **
   ******************/

  def tweetLength(text: String): Int =
    text.codePointCount(0, text.length)

  test("tweetRemainingCharsCount with a constant signal") {
    val result = TweetLength.tweetRemainingCharsCount(Var("hello world"))
    assert(result() == MaxTweetLength - tweetLength("hello world"))

    val tooLong = "foo" * 200
    val result2 = TweetLength.tweetRemainingCharsCount(Var(tooLong))
    assert(result2() == MaxTweetLength - tweetLength(tooLong))
  }

  test("tweetRemainingCharsCount with a supplementary char") {
    val result = TweetLength.tweetRemainingCharsCount(Var("foo blabla \uD83D\uDCA9 bar"))
    assert(result() == MaxTweetLength - tweetLength("foo blabla \uD83D\uDCA9 bar"))
  }


  test("colorForRemainingCharsCount with a constant signal") {
    val resultGreen1 = TweetLength.colorForRemainingCharsCount(Var(52))
    assert(resultGreen1() == "green")
    val resultGreen2 = TweetLength.colorForRemainingCharsCount(Var(15))
    assert(resultGreen2() == "green")

    val resultOrange1 = TweetLength.colorForRemainingCharsCount(Var(12))
    assert(resultOrange1() == "orange")
    val resultOrange2 = TweetLength.colorForRemainingCharsCount(Var(0))
    assert(resultOrange2() == "orange")

    val resultRed1 = TweetLength.colorForRemainingCharsCount(Var(-1))
    assert(resultRed1() == "red")
    val resultRed2 = TweetLength.colorForRemainingCharsCount(Var(-5))
    assert(resultRed2() == "red")
  }

  /******************
   ** POLYNOMIAL **
   ******************/

  test("computeDelta") {
    val a = Var(1.0)
    var b = Var(6.0)
    val c = Var(9.0)
    assert(Polynomial.computeDelta(a, b, c)() === 0.0)
    b = Var(7.0)
    assert(Polynomial.computeDelta(a, b, c)() === 13.0)
  }
  
  test("computeSolutions") {
    val a = Var(1.0)
    var b = Var(6.0)
    val c = Var(9.0)
    val delta = Var(0.0)
    assert(Polynomial.computeSolutions(a, b, c, delta)() == Set(-3.0))
  }
  
  test("computeValues") {
    val inputMap: Map[String, Signal[Expr]] = Map(
    				     ("a", Signal(Plus(Ref("a"), Literal(1.0)))),
                 ("b", Signal(Literal(0.0))),
                 ("c", Signal(Literal(0.0))),
                 ("d", Signal(Literal(0.0))),
                 ("e", Signal(Literal(0.0))),
                 ("f", Signal(Literal(0.0))),
                 ("g", Signal(Literal(0.0))),
                 ("h", Signal(Literal(0.0))),
                 ("i", Signal(Literal(0.0))),
                 ("j", Signal(Literal(0.0)))
              )
    Calculator.computeValues(inputMap)
  }
}
