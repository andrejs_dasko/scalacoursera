package quickcheck

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

object magGenWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  lazy val genMap: Gen[Map[Int,Int]] = for {
	  k <- arbitrary[Int]
	  v <- arbitrary[Int]
	  m <- oneOf(const(Map.empty[Int,Int]), genMap)
	} yield m.updated(k, v)                   //> genMap  : org.scalacheck.Gen[Map[Int,Int]] = <lazy>
	
	genMap.sample                             //> res0: Option[Map[Int,Int]] = Some(Map(-688692685 -> 1, -344775539 -> -1, 1 -
                                                  //| > 0))
}