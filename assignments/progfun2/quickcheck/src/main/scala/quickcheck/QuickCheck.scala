package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] =  for {
    nextElement <- arbitrary[Int]
    heap <- oneOf(const(empty), genHeap)
  } yield insert(nextElement, heap)
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }
  
  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }
  /* If you insert any two elements into an empty heap, 
   * finding the minimum of the resulting heap should 
   * get the smallest of the two elements back.*/
  property("gen2") = forAll { (h: H) =>
    val lesser = 2
    val greater = 4
    val heap = insert(greater, (insert(lesser, empty)))
    findMin(heap) == lesser
  }
  
  /* If you insert an element into an empty heap, then 
   * delete the minimum, the resulting heap should be empty.*/
  property("gen3") = forAll { (h: H) =>
    val i = 2
    val heap = insert(i, empty)
    deleteMin(heap) == empty
  }
  
  def validateHeap(heap: H, acc: List[Int]): Boolean = {
    if (isEmpty(heap)) true 
    else {
      if (acc == acc.sortWith(_ < _))
        validateHeap(deleteMin(heap), findMin(heap) :: acc)
      else false
    }
  }
  
  property("gen4") = forAll { (h: H) =>
    validateHeap(h, List())
  }
  
  /* Finding a minimum of the melding of any two heaps 
   * should return a minimum of one or the other.*/
  property("gen5") = forAll { (h: H) =>
    val heap1 = insert(6, (insert(7, empty)))
    val heap2 = insert(5, (insert(3, empty)))
    findMin(meld(heap1, heap2)) == 3
  }
}