package reductions

import scala.annotation._
import org.scalameter._
import common._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true) withWarmer (new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }
}

object ParallelParenthesesBalancing {

  /**
   * Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
    def loop(bal: Int, cs: Array[Char]): Boolean = {
      // exit conditions
      if (cs.isEmpty) bal == 0
      else if (bal < 0) false
      
      // recursive calls
      else if (cs.head == '(') loop(bal + 1, cs.tail)
      else if (cs.head == ')') loop(bal - 1, cs.tail)
      else loop(bal, cs.tail)
    }
    loop(0, chars)
  }

  /**
   * Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(idx: Int, until: Int) /*: ???*/ = {
      def loop(bal: Int, correct: Boolean, cs: Array[Char]): (Int, Boolean) = {
        // exit conditions
        if (cs.isEmpty) (bal, correct)
        
        // recursive calls
        else if (cs.head == '(') {
          val nextBal = bal + 1
          loop(nextBal, correct && (nextBal >= 0), cs.tail) 
        }
        else if (cs.head == ')') {
          val nextBal = bal - 1
          loop(nextBal, correct && (nextBal >= 0), cs.tail)
        }
        else loop(bal, correct, cs.tail) 
      }
      loop(0, true, chars.slice(idx, until))
    }

    def reduce(from: Int, until: Int): (Int, Boolean) = {
      if (until - from <= threshold) {
        // sequential computation
        traverse(from, until)
      } else {
        val mid = from + (until - from) / 2
        val ((leftBal, leftCorrect), (rightBal, rightCorrect)) = parallel(
            reduce(from, mid),
            reduce(mid, until))
        
        val commonBalance = leftBal + rightBal
        if (commonBalance >= 0) (commonBalance, leftCorrect) else (commonBalance, false)
      }
    }

    val (bal, correct) = reduce(0, chars.length)
    correct && bal == 0
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
