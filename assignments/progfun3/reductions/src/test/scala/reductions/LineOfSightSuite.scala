package reductions

import java.util.concurrent._
import scala.collection._
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import common._
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory

@RunWith(classOf[JUnitRunner]) 
class LineOfSightSuite extends FunSuite {
  import LineOfSight._
  test("lineOfSight should correctly handle an array of size 4") {
    val output = new Array[Float](4)
    lineOfSight(Array[Float](0f, 1f, 8f, 9f), output)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }

  test("upsweepSequential should correctly handle the chunk 1 until 4 of an array of 4 elements") {
    val res = upsweepSequential(Array[Float](0f, 1f, 8f, 9f), 1, 4)
    assert(res == 4f)
  }
  
  test("upsweep should correctly handle the chunk 1 until 4 of an array of 4 elements") {
    val input = Array[Float](0f, 1f, 8f, 9f)
    val res = upsweep(input, 0, input.length, 1)
    val expected = Node(Node(Leaf(0,1,0.0f),Leaf(1,2,1.0f)),Node(Leaf(2,3,4.0f),Leaf(3,4,3.0f)))
    assert(res == expected)
  }

  test("downsweepSequential should correctly handle a 4 element array when the starting angle is zero") {
    val output = new Array[Float](4)
    downsweepSequential(Array[Float](0f, 1f, 8f, 9f), output, 0f, 1, 4)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }
  
  test("downsweep should correctly handle a 4 element array when the starting angle is zero") {
    val input = Array[Float](0f, 1f, 8f, 9f)
    val output = new Array[Float](4)
    val upsweepRes = upsweep(input, 1, input.length, 1)
    downsweep(input, output, 0f, upsweepRes)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }
  
  test("downsweep should correctly compute the output for a non-zero starting angle") {
    val input = Array[Float](0f, 1f, 8f, 9f)
    val output = new Array[Float](4)
    val upsweepRes = upsweep(input, 1, input.length, 1)
    downsweep(input, output, 5f, upsweepRes)
    assert(output.toList == List(0f, 5f, 5f, 5f))
  }
  
  test("downsweep should correctly compute the output for a tree with 4 leaves when the starting angle is zero") {
    val input = Array[Float](0f, 7f, 14f, 33f, 48f)
    val output = new Array[Float](input.length)
    val upsweepRes = upsweep(input, 1, input.length, 1)
    downsweep(input, output, 0f, upsweepRes)
    assert(output.toList == List(0f, 7f, 7f, 11f, 12f))
  }
}

