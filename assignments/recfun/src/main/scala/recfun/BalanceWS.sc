package recfun

object BalanceWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def balance(chars: List[Char]): Boolean = {
  	def loop(bal: Int, cs: List[Char]): Boolean = {
  			if (cs.isEmpty) bal == 0
  			else if (bal < 0) false
  			else if (cs.head == '(') loop(bal + 1, cs.tail)
  			else if (cs.head == ')') loop(bal - 1, cs.tail)
  			else loop(bal, cs.tail)
  	}
  	loop(0, chars)
  }                                               //> balance: (chars: List[Char])Boolean
  balance("(if (zero? x) max (/ 1 x))".toList)    //> res0: Boolean = true
  balance("I told him (that it’s not (yet) done). (But he wasn’t listening)".toList)
                                                  //> res1: Boolean = true
  balance(":-)".toList)                           //> res2: Boolean = false
  balance("())(".toList)                          //> res3: Boolean = false
}