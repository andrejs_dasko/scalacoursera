package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  	def pascal(c: Int, r: Int): Int = {
  	  if (c > r || c < 0 || r < 0) 0
  	  else if (c == 0 || c == r) 1
  	  else pascal(c, r - 1) + pascal(c - 1, r - 1)
  	} 
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {
    	def loop(bal: Int, cs: List[Char]): Boolean = {
    			if (cs.isEmpty) bal == 0
    			else if (bal < 0) false
    			else if (cs.head == '(') loop(bal + 1, cs.tail)
    			else if (cs.head == ')') loop(bal - 1, cs.tail)
    			else loop(bal, cs.tail)
    	}
    	loop(0, chars)
    }
  
  /**
   * Exercise 3
   */
	def countChange(money: Int, coins: List[Int]): Int = {
		def loop(money: Int, coins: List[Int]): Int = {
		  if (coins.isEmpty) 0
		  else if (money == 0) 1
		  else if (money < coins.head) countChange(money, coins.tail)
		  else if (money == coins.head) 1 + countChange(money, coins.tail)
		  else countChange(money - coins.head, coins) + countChange(money, coins.tail) // money > coins.head
		}
		loop(money, coins.sortWith(_ > _))
	}  
}