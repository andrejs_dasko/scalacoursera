package recfun

object PascalWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
	def pascal(c: Int, r: Int): Int = {
	  if (c > r || c < 0 || r < 0) 0
	  else if (c == 0 || c == r) 1
	  else pascal(c, r - 1) + pascal(c - 1, r - 1)
	}                                         //> pascal: (c: Int, r: Int)Int
	
	pascal(2, 5)                              //> res0: Int = 10
}