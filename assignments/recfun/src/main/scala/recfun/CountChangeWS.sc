package recfun

object CountChangeWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
	def countChange(money: Int, coins: List[Int]): Int = {
		def loop(money: Int, coins: List[Int]): Int = {
		  if (money == 0 || coins.isEmpty || money < coins.head) 0
		  else if (money == coins.head) 1
		  else countChange(money - coins.head, coins.tail) + countChange(money, coins.tail) // money > coins.head
		}
		loop(money, coins.sortWith(_ > _))
	}                                         //> countChange: (money: Int, coins: List[Int])Int
	
	val x: List[Int] = List(2, 1, 3)          //> x  : List[Int] = List(2, 1, 3)
	x.sortWith(_ > _)                         //> res0: List[Int] = List(3, 2, 1)
}