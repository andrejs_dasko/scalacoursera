package example

object ListsWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def sum(xs: List[Int]): Int = if (xs.isEmpty) 0 else xs.head + sum(xs.tail)
                                                  //> sum: (xs: List[Int])Int
  
  val x: List[Int] = List(1, 3, 2)                //> x  : List[Int] = List(1, 3, 2)
  
  sum(x)                                          //> res0: Int = 6
  
	def max(xs: List[Int]): Int = {
	  def loop(max: Int, xss: List[Int]): Int = {
	    if (xss.isEmpty)
	      max
	    else {
	      if (xss.head > max) loop(xss.head, xss.tail) else loop(max, xss.tail)
	    }
	  }
	  loop(0, xs)
	}                                         //> max: (xs: List[Int])Int
	
	max(x)                                    //> res1: Int = 3
}