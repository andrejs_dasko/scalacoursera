package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
	trait TestTrees {
		val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
		val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
	}


  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  test("times") {
    new TestTrees {
      val extracted = times(List('a','b','a','a','b','b','a'))
      assert(extracted === List(('a',4), ('b',3)))
      assert(extracted contains ('a',4))
      assert(extracted contains ('b',3))
      assert(extracted.size === 2)
    }
  }
  
  test("sort frequency table") {
    new TestTrees {
      val testSubject = List(('a',4), ('b',3), ('c', 7), ('g',1))
//      println(testSubject)
//      println(testSubject.sortWith((x, y) => x._2 < y._2))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }


  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }


  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }


  test("until(singleton, combine)(trees)") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4)) 
    assert(until(singleton, combine)(leaflist) === new Fork(new Fork(new Leaf('e', 1), new Leaf('t', 2), List('e', 't'), 3), new Leaf('x', 4), List('e', 't', 'x'), 7))
    val leaflist2 = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 2))
    assert(until(singleton, combine)(leaflist2) === new Fork(new Leaf('x', 2), new Fork(new Leaf('e', 1), new Leaf('t', 2), List('e', 't'), 3), List('x', 'e', 't'), 5))
    //println(until(singleton, combine)(makeOrderedLeafList(times(string2Chars("abcabbab")))))
  }


  test("decode") {
    println(decode(frenchCode, secret))
//    val tree = until(singleton, combine)(makeOrderedLeafList(times(string2Chars("aabbbc"))))
//    println(tree)
//    println(decode(tree, List(1,0,0,0,1,0,1,0,0)))
  }


  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

}
