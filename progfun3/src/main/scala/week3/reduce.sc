package week3

object reduce {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  sealed trait Tree[A]
  case class Leaf[A](v: A) extends Tree[A]
  case class Node[A](left: Tree[A], right: Tree[A]) extends Tree[A]
  
  val t1 = Node(Leaf(1), Node(Leaf(3), Leaf(8)))  //> t1  : week3.reduce.Node[Int] = Node(Leaf(1),Node(Leaf(3),Leaf(8)))
  
  def reduce[A](tree: Tree[A], f: (A, A) => A): A = tree match {
  	case Leaf(v) => v
  	case Node(l, r) => f(reduce[A](l, f), reduce[A](r, f))
  }                                               //> reduce: [A](tree: week3.reduce.Tree[A], f: (A, A) => A)A
  
  reduce(t1, (a: Int, b: Int) => a + b)           //> res0: Int = 12
  reduce(t1, (a: Int, b: Int) => a - b)           //> res1: Int = 6
  
  def map[A,B](tree: Tree[A], f: (A) => B): Tree[B] = tree match {
  	case Leaf(v) => Leaf(f(v))
  	case Node(l, r) => Node(map[A,B](l, f), map[A,B](r, f))
  }                                               //> map: [A, B](tree: week3.reduce.Tree[A], f: A => B)week3.reduce.Tree[B]
  
  map(t1, (a: Int) => a + 1)                      //> res2: week3.reduce.Tree[Int] = Node(Leaf(2),Node(Leaf(4),Leaf(9)))
  
  def toList[A](tree: Tree[A]): List[A] = tree match {
  	case Leaf(v) => List(v)
  	case Node(l, r) => toList(l) ++ toList(r)
  }                                               //> toList: [A](tree: week3.reduce.Tree[A])List[A]
  toList(t1)                                      //> res3: List[Int] = List(1, 3, 8)
  
  // val t2 = t1.map(List(_))
  val map1 = map(t1, (a: Int) => List(a))         //> map1  : week3.reduce.Tree[List[Int]] = Node(Leaf(List(1)),Node(Leaf(List(3))
                                                  //| ,Leaf(List(8))))
  // t2.reduce(_ ++ _)
  val reduce1 = reduce(map1, (a: List[Int], b: List[Int]) => a ++ b)
                                                  //> reduce1  : List[Int] = List(1, 3, 8)
                                                  
  //val toList = reduce(map(t1, List(_)), _ ++ _)
  
  // demonstarting associativity with custom map and reduce
  val ta = Node(Leaf(1), Node(Leaf(3), Leaf(8)))  //> ta  : week3.reduce.Node[Int] = Node(Leaf(1),Node(Leaf(3),Leaf(8)))
  val tb = Node(Node(Leaf(1), Leaf(3)), Leaf(8))  //> tb  : week3.reduce.Node[Int] = Node(Node(Leaf(1),Leaf(3)),Leaf(8))
  // reduce(map(ta, List(_)), _ ++ _) == reduce(map(tb, List(_)), _ ++ _)
  
  val treshhold = 2                               //> treshhold  : Int = 2
  
  def reduceSeq[A](arr: Array[A], left: Int, right: Int, f: (A, A) => A): A = {
  	if (right - left <= treshhold) {
  		var result = arr(left)
  		for {
  			i <- (left + 1) until right
  		} result = f(result, arr(i))
  		result
  	} else {
  		val mid = left + (right - left) / 2
  		// val (a, b) = parallel(reduceSeq(arr, left, mid, f), reduceSeq(arr, mid, right, f))
  		val a = reduceSeq(arr, left, mid, f)
  		val b = reduceSeq(arr, mid, right, f)
  		f(a, b)
  	}
  }                                               //> reduceSeq: [A](arr: Array[A], left: Int, right: Int, f: (A, A) => A)A
  
  val ar1 = Array(1, 2, 3, 4, 5, 6)               //> ar1  : Array[Int] = Array(1, 2, 3, 4, 5, 6)
  val sum = (a: Int, b: Int) => a + b             //> sum  : (Int, Int) => Int = <function2>
  
  reduceSeq(ar1, 3, 6, sum)                       //> res4: Int = 15
}