package week3

object SeqScanLeftWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val inp = Array(1, 3, 8)                        //> inp  : Array[Int] = Array(1, 3, 8)
  val out = Array(0, 0, 0, 0)                     //> out  : Array[Int] = Array(0, 0, 0, 0)
  val seed = 100                                  //> seed  : Int = 100
  def f = (seed: Int, el: Int) => seed + el       //> f: => (Int, Int) => Int
  
  def scanLeft[A](inp: Array[A],
                  seed: A,
                  f: (A, A) => A,
                  out: Array[A]): Unit = {
    out(0) = seed
    for {
      i <- 0 until inp.length
    } out(i + 1) = f(out(i), inp(i))
  }                                               //> scanLeft: [A](inp: Array[A], seed: A, f: (A, A) => A, out: Array[A])Unit
  
  scanLeft(inp, seed, f, out)
  out                                             //> res0: Array[Int] = Array(100, 101, 104, 112)
}