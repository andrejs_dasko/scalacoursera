package week3

object TreesToTreeResWS {
  sealed abstract class Tree[A]
  case class Leaf[A](a: A) extends Tree[A]
  case class Node[A](l: Tree[A], r: Tree[A]) extends Tree[A]

  sealed abstract class TreeRes[A] { val res: A }
  case class LeafRes[A](override val res: A) extends TreeRes[A]
  case class NodeRes[A](l: TreeRes[A],
                        override val res: A,
                        r: TreeRes[A]) extends TreeRes[A]

  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  def upSweep[A](t: Tree[A], f: (A, A) => A): TreeRes[A] = t match {
    case Leaf(v)                  => LeafRes(v)
    case Node(l, r) => {
      val leftReduced = upSweep(l, f)
      val rightReduced = upSweep(r, f)
      //val (leftReduced, rightReduced) = parallel(reduceRes(l, f), reduceRes(r, f))
      NodeRes(leftReduced, f(leftReduced.res, rightReduced.res), rightReduced)
    }
  }                                               //> upSweep: [A](t: week3.TreesToTreeResWS.Tree[A], f: (A, A) => A)week3.TreesTo
                                                  //| TreeResWS.TreeRes[A]

  val tree = Node(Node(Leaf(1), Leaf(2)), Node(Leaf(3), Leaf(8)))
                                                  //> tree  : week3.TreesToTreeResWS.Node[Int] = Node(Node(Leaf(1),Leaf(2)),Node(L
                                                  //| eaf(3),Leaf(8)))
  val sum = (a: Int, b: Int) => a + b             //> sum  : (Int, Int) => Int = <function2>
  upSweep(tree, sum)                              //> res0: week3.TreesToTreeResWS.TreeRes[Int] = NodeRes(NodeRes(LeafRes(1),3,Le
                                                  //| afRes(2)),14,NodeRes(LeafRes(3),11,LeafRes(8)))
                                                  
	val tree2 = Node(Node(Node(Leaf(4), Leaf(5)), Leaf(2)), Node(Leaf(3), Leaf(8)))
                                                  //> tree2  : week3.TreesToTreeResWS.Node[Int] = Node(Node(Node(Leaf(4),Leaf(5))
                                                  //| ,Leaf(2)),Node(Leaf(3),Leaf(8)))
	upSweep(tree2, sum)                       //> res1: week3.TreesToTreeResWS.TreeRes[Int] = NodeRes(NodeRes(NodeRes(LeafRes
                                                  //| (4),9,LeafRes(5)),11,LeafRes(2)),22,NodeRes(LeafRes(3),11,LeafRes(8)))

	def downSweep[A](t: TreeRes[A], seed: A, f: (A, A) => A): Tree[A] = t match {
		case LeafRes(res) => Leaf(f(seed, res))
		case NodeRes(l, _, r) => {
			val leftDownSweep = downSweep(l, seed, f)
			val rightDownSweep = downSweep(r, f(seed, l.res), f)
			// val (leftDownSweep, rightDownSweep = parallel(downSweep(l, seed, f), downSweep(r, f(seed, l.res), f))
			Node(leftDownSweep, rightDownSweep)
		}
	}                                         //> downSweep: [A](t: week3.TreesToTreeResWS.TreeRes[A], seed: A, f: (A, A) => 
                                                  //| A)week3.TreesToTreeResWS.Tree[A]
  downSweep(upSweep(tree2, sum), 100, sum)        //> res2: week3.TreesToTreeResWS.Tree[Int] = Node(Node(Node(Leaf(104),Leaf(109)
                                                  //| ),Leaf(111)),Node(Leaf(114),Leaf(122)))
                      
	def prepend[A](elem: A, t: Tree[A]): Tree[A] = t match {
		case Node(l, r) => Node(prepend(elem, l), r)
		case Leaf(v) => Node(Leaf(elem), Leaf(v))
	}                                         //> prepend: [A](elem: A, t: week3.TreesToTreeResWS.Tree[A])week3.TreesToTreeRe
                                                  //| sWS.Tree[A]
	 
	                            
	def scanLeft[A](t: Tree[A], seed: A, f: (A, A) => A): Tree[A] = {
		val tRes = upSweep(t, f)
		val scan1 = downSweep(tRes, seed, f)
		prepend(seed, scan1)
	}                                         //> scanLeft: [A](t: week3.TreesToTreeResWS.Tree[A], seed: A, f: (A, A) => A)we
                                                  //| ek3.TreesToTreeResWS.Tree[A]
  scanLeft(tree2, 100, sum)                       //> res3: week3.TreesToTreeResWS.Tree[Int] = Node(Node(Node(Node(Leaf(100),Leaf
                                                  //| (104)),Leaf(109)),Leaf(111)),Node(Leaf(114),Leaf(122)))
}