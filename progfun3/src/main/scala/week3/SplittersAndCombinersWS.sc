package main.scala.week3

object SplittersAndCombinersWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  trait Iterator {
		def next(): Int
		def hasNext: Boolean
		def foldLeft(z: Int)(f: (Int, Int) => Int): Int = {
			if (hasNext) {
				foldLeft(f(z, next()))(f)
			} else {
				z
			}
		}
	}
	case class MyIterator(list: List[Int]) extends Iterator {
		var myList = list
	
		def next: Int = {
			myList = myList.tail
			myList.head
		}
		
		def hasNext: Boolean = list.length > 0
	}
	
	MyIterator(List(1, 2, 3, 4, 5, 6)).foldLeft(1)((a: Int, b: Int) => a * b)
                                                  //> java.util.NoSuchElementException: head of empty list
                                                  //| 	at scala.collection.immutable.Nil$.head(List.scala:420)
                                                  //| 	at scala.collection.immutable.Nil$.head(List.scala:417)
                                                  //| 	at main.scala.week3.SplittersAndCombinersWS$$anonfun$main$1$MyIterator$3
                                                  //| .next(main.scala.week3.SplittersAndCombinersWS.scala:22)
                                                  //| 	at main.scala.week3.SplittersAndCombinersWS$$anonfun$main$1$Iterator$1$c
                                                  //| lass.foldLeft(main.scala.week3.SplittersAndCombinersWS.scala:11)
                                                  //| 	at main.scala.week3.SplittersAndCombinersWS$$anonfun$main$1$MyIterator$3
                                                  //| .foldLeft(main.scala.week3.SplittersAndCombinersWS.scala:17)
                                                  //| 	at main.scala.week3.SplittersAndCombinersWS$$anonfun$main$1$Iterator$1$c
                                                  //| lass.foldLeft(main.scala.week3.SplittersAndCombinersWS.scala:11)
                                                  //| 	at main.scala.week3.SplittersAndCombinersWS$$anonfun$main$1$MyIterator$3
                                                  //| .foldLeft(main.scala.week3.SplittersAndCombinersWS.scala:17)
                                                  //| 	at main.scala.week3.SplittersAndCombinersWS$$anonfun$main$1$Iterator$1$c
                                                  //| lass.foldLef
                                                  //| Output exceeds cutoff limit.
}