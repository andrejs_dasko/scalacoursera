package week3

object SeqScanLeft {
  def scanLeft[A](inp: Array[A],
                  seed: A,
                  f: (A, A) => A,
                  out: Array[A]): Unit = {
    out(0) = seed
    for {
      i <- 0 to inp.length
    } out(i + 1) = f(out(i), inp(i)) 
  }
}