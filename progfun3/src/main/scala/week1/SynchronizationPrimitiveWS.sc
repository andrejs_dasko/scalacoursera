package week1

object SynchronizationPrimitiveWS {

  var uniqueId = 0                                //> uniqueId  : Int = 0
  val x = new AnyRef()                            //> x  : Object = java.lang.Object@3b6eb2ec

  def getUniqueId = x.synchronized {
    uniqueId = uniqueId + 1
    uniqueId
  }                                               //> getUniqueId: => Int

  def startThread = {
    val t = new Thread {
      override def run() {
        val uuids = for (i <- (0 until 10)) yield getUniqueId
        println(uuids)
      }
    }
    t.start
  }                                               //> startThread: => Unit
  
  startThread
  
  def sumSegment(a: Array[Int], p: Double, s: Int, t: Int): Int = {
  	def loop(sCur: Int, acc: Int): Int = {
  		if (sCur == t) acc
  		else loop(sCur + 1, acc + Math.pow(a(sCur), p).toInt)
  	}
  	loop(s, 0)
  }                                               //> sumSegment: (a: Array[Int], p: Double, s: Int, t: Int)Int
  
  def sumSegment2(a: Array[Int], p: Int, s: Int, t: Int): Int = {
  	def f(num: Int, power: Int): Int = Math.pow(num, power).toInt
  	val sumList = for {
  		i <- 0 until t
  	} yield f(a(i), p)
  	//sumList.foldLeft(0)((acc, x) => acc + x)
  	sumList.foldLeft(0)(_ + _)
  	// sumList flatMap((a, b) => a + b)
  }                                               //> sumSegment2: (a: Array[Int], p: Int, s: Int, t: Int)Int
  
  val arr = Array(2, 3, 4)                        //> Vector(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                                                  //| arr  : Array[Int] = Array(2, 3, 4)
  sumSegment(arr, 2, 0, 3)                        //> res0: Int = 29
  sumSegment(arr, 2, 0, 3)                        //> res1: Int = 29
}