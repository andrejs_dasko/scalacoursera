package greeter

object testsheet {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(79); 
  println("Welcome to the Scala worksheet");$skip(12); 
  val x = 1;System.out.println("""x  : Int = """ + $show(x ));$skip(31); 
  def increase(i: Int) = i + 1;System.out.println("""increase: (i: Int)Int""");$skip(14); val res$0 = 
  increase(x);System.out.println("""res0: Int = """ + $show(res$0));$skip(14); val res$1 = 
  increase(x);System.out.println("""res1: Int = """ + $show(res$1));$skip(46); 
  
  def abs(x:Double) = if (x < 0) -x else x;System.out.println("""abs: (x: Double)Double""");$skip(38); 

  def sqr(x: Double): Double = x * x;System.out.println("""sqr: (x: Double)Double""");$skip(127); 

	def sqrtIter(guess: Double, x: Double): Double =
	  if (isGoodEnough(guess, x)) guess
	  else sqrtIter(improve(guess, x), x);System.out.println("""sqrtIter: (guess: Double, x: Double)Double""");$skip(84); 
	
	def isGoodEnough(guess: Double, x: Double) =
		abs(sqr(guess) - x) / x < 0.00001;System.out.println("""isGoodEnough: (guess: Double, x: Double)Boolean""");$skip(144); 
		//sqr(abs(x - guess)) < 0.00001
		
	
	def improve(guess: Double, x: Double) =  {
		//println("guess: " + guess)
		(guess + x / guess) / 2
	};System.out.println("""improve: (guess: Double, x: Double)Double""");$skip(42); 
	
	def sqrt(x: Double) = sqrtIter(1.0, x);System.out.println("""sqrt: (x: Double)Double""");$skip(13); val res$2 = 
	
	
	sqrt(4);System.out.println("""res2: Double = """ + $show(res$2));$skip(14); val res$3 = 
	
	sqrt(1e-6);System.out.println("""res3: Double = """ + $show(res$3));$skip(13); val res$4 = 
  sqrt(1e60);System.out.println("""res4: Double = """ + $show(res$4))}
}
