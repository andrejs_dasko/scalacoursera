package greeter

object testsheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val x = 1                                       //> x  : Int = 1
  def increase(i: Int) = i + 1                    //> increase: (i: Int)Int
  increase(x)                                     //> res0: Int = 2
  increase(x)                                     //> res1: Int = 2
  
  def abs(x:Double) = if (x < 0) -x else x        //> abs: (x: Double)Double

  def sqr(x: Double): Double = x * x              //> sqr: (x: Double)Double

	def sqrtIter(guess: Double, x: Double): Double =
	  if (isGoodEnough(guess, x)) guess
	  else sqrtIter(improve(guess, x), x)     //> sqrtIter: (guess: Double, x: Double)Double
	
	def isGoodEnough(guess: Double, x: Double) =
		abs(sqr(guess) - x) / x < 0.00001 //> isGoodEnough: (guess: Double, x: Double)Boolean
		//sqr(abs(x - guess)) < 0.00001
		
	
	def improve(guess: Double, x: Double) =  {
		//println("guess: " + guess)
		(guess + x / guess) / 2
	}                                         //> improve: (guess: Double, x: Double)Double
	
	def sqrt(x: Double) = sqrtIter(1.0, x)    //> sqrt: (x: Double)Double
	
	
	sqrt(4)                                   //> res2: Double = 2.0000000929222947
	
	sqrt(1e-6)                                //> res3: Double = 0.0010000001533016628
  sqrt(1e60)                                      //> res4: Double = 1.0000000031080746E30
}