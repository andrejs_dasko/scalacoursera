package week4

object naturals {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val one = new Succ(Zero)                        //> one  : week4.Succ = |
  val two = new Succ(new Succ(Zero))              //> two  : week4.Succ = ||
  val sumThree = one + two                        //> sumThree  : week4.Nat = |||
  val sumTwo = one + one                          //> sumTwo  : week4.Nat = ||
  val subOne = two - one                          //> subOne  : week4.Nat = |
  val subZero = two - two                         //> subZero  : week4.Nat = 
  two.successor                                   //> res0: week4.Nat = |||
  two.predecessor                                 //> res1: week4.Nat = |
  Zero.successor                                  //> res2: week4.Nat = |
  val five = two + two + one                      //> five  : week4.Nat = |||||
  val zero = five - two - one - two               //> zero  : week4.Nat = 
  //Zero.predecessor
  //val subLessThanZero = one - two
  
  class AnonFun extends Function1[Int, Int] {
		def apply(x: Int): Int = x * x
		new AnonFun
	}
	
	// representation of an anonymous function value
	new Function1[Int, Int] {
		def apply(x: Int): Int = x * x
	}                                         //> res3: Int => Int = <function1>
}

abstract class Nat {
	def isZero: Boolean
	def predecessor: Nat
	def successor: Nat = new Succ(this)
	def + (that: Nat): Nat
	def - (that: Nat): Nat
}

object Zero extends Nat {
	def isZero = true
	def predecessor = throw new NoSuchElementException("not a natural number")
	def + (that: Nat) = that
	def - (that: Nat) = if (that.isZero) this else throw new NoSuchElementException("not a natural number")
	
	override def toString = ""
}
class Succ(n: Nat) extends Nat {
	def isZero = false
	def predecessor = n
	def + (that: Nat) = n + that.successor // or new Succ(n + that)
	def - (that: Nat) = if (that.isZero) this else n - that.predecessor
	
	override def toString = "|" + n
}