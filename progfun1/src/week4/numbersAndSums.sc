package week4

object numbersAndSums {
  println("Welcome to the Scala worksheet")
  
  def eval(e: Expr): Int = {
  	if (e.isNumber) e.numValue
  	else if (e.isSum) eval(e.leftOp) + eval(e.rightOp)
  	else throw new Error("not implemented Expr")
  }
}

trait Expr {
	def isNumber: Boolean
	def isSum: Boolean
	def numValue: Int
	def leftOp: Expr
	def rightOp: Expr
}

class Num(x: Int) extends Expr {
	def isNumber: Boolean = true
	def isSum: Boolean = false
	def numValue: Int = x
	def leftOp: Expr = throw new Error("Num.leftOp")
	def rightOp: Expr = throw new Error("Num.rightOp")
}

class Sum(e1: Expr, e2: Expr) extends Expr {
	def isNumber: Boolean = false
	def isSum: Boolean = true
	def numValue: Int = throw new Error("Sum.numValue")
	def leftOp: Expr = e1
	def rightOp: Expr = e2
}

class Prod(e1: Expr, e2: Expr) extends Expr
class Var(x: String) extends Expr