package week4

object lists {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  List()                                          //> res0: week4.List[Nothing] = .
  List(1)                                         //> res1: week4.List[Int] = 1 -> .
  List(1, 2)                                      //> res2: week4.List[Int] = 1 -> 2 -> .
  List(1, 2, 3)                                   //> res3: week4.List[(Int, Int, Int)] = (1,2,3) -> .
  List(1, 2, 3, 4)                                //> res4: week4.List[(Int, Int, Int, Int)] = (1,2,3,4) -> .
}

trait List[T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty: Boolean = false
  override def toString = head + " -> " + tail
}

class Nil[T] extends List[T] {
  def isEmpty: Boolean = true
  def head: Nothing = throw new NoSuchElementException("Nil.head")
  def tail: Nothing = throw new NoSuchElementException("Nil.tail")
  override def toString = "."
}

object List {
	def apply[T](): List[T] 						 = new Nil
	def apply[T](x: T): List[T] 				= new Cons(x, new Nil)
	def apply[T](x: T, y: T): List[T] 	= new Cons(x, new Cons(y, new Nil))
}