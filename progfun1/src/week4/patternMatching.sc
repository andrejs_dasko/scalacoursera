package week4

object patternMatching {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  
	def eval(e: Expr): Int = e match {
		case Number(n) => n
		case Summ(e1, e2) => eval(e1) + eval(e2)
	}                                         //> eval: (e: week4.Expr)Int
	
	def show(e: Expr): String = e match {
		case Number(n) => n.toString
		case Summ(e1, e2) => show(e1) + " + " + show(e2)
		case Summ(Product(e1, e2), Number(x)) => show(e1) + " * " + show(e2) + " + " + x
		case Product(Summ(e1, e2), Number(x)) => "(" + show(e1) + " + " + show(e2) + ") * " + x
		case Product(Number(x), Number(y)) => x + " * " + y
	}                                         //> show: (e: week4.Expr)String
  
  val x = new Summ(new Number(1), new Summ(new Number(2), new Number(3)))
                                                  //> x  : week4.Summ = Summ(Number(1),Summ(Number(2),Number(3)))
   
  eval(x)                                         //> res0: Int = 6
  show(x)                                         //> res1: String = 1 + 2 + 3
  show(new Summ(new Product(new Number(2), new Number(5)), new Number(7)))
                                                  //> res2: String = 2 * 5 + 7
  show(new Product(new Summ(new Number(2), new Number(5)), new Number(7)))
                                                  //> res3: String = (2 + 5) * 7
}

trait Expr
case class Number(x: Int) extends Expr
case class Summ(e1: Expr, e2: Expr) extends Expr
case class Product(e1: Expr, e2: Expr) extends Expr