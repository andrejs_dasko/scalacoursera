package week2

class Rational(x: Int, y: Int) {
	require(y != 0, "Denominator must be non-zero")

	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
	private val g = gcd(x, y)
	val numer = x / g
	val denom = y / g
	
	def add(r: Rational) = new Rational(r.numer * denom + numer * r.denom, r.denom * denom)
	
	def neg = new Rational(-numer, denom)
	
	def sub(r: Rational) = add(r.neg) // same as this.add(r.neg)
	
	def less(that: Rational): Boolean = numer * that.denom < that.numer * denom
	
	def max(that: Rational): Rational = if (this.less(that)) that else this
	
	override def toString = numer + "/" + denom
}