package week2

object ProductWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def summ(f: Int => Int)(a: Int, b: Int): Int = {
  	if (a > b) 0 else f(a) + summ(f)(a + 1, b)
  }                                               //> summ: (f: Int => Int)(a: Int, b: Int)Int
  
  def product(f: Int => Int)(a: Int, b: Int): Int = {
  	if (a > b) 1 else f(a) * product(f)(a + 1, b)
  }                                               //> product: (f: Int => Int)(a: Int, b: Int)Int
  
  product(x => x * x)(1, 5)                       //> res0: Int = 14400
  
  def fact(n: Int): Int = product(x => x)(1, n)   //> fact: (n: Int)Int
  
  fact(4)                                         //> res1: Int = 24
  
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int, b: Int): Int = {
  	if (a > b) unit
  	else combine(f(a), mapReduce(f, combine, unit)(a + 1, b))
  }                                               //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int, b:
                                                  //|  Int)Int
  
  mapReduce(x => x * x, (x, y) => x + y, 0)(1, 4) //> res2: Int = 30
  mapReduce(x => x, (x, y) => x * y, 1)(1, 4)     //> res3: Int = 24
  
  def product2(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x * y, 1)(a, b)
                                                  //> product2: (f: Int => Int)(a: Int, b: Int)Int
  def sum2(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x + y, 0)(a, b)
                                                  //> sum2: (f: Int => Int)(a: Int, b: Int)Int
  
  product2(x => x)(1, 5)                          //> res4: Int = 120
  sum2(x => x)(1, 5)                              //> res5: Int = 15
}