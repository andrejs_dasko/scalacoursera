package week2

object SumTailRecustionWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  def sum(f: Int => Int)(a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, f(a) + acc)
    }
    loop(a, 0)
  }                                               //> sum: (f: Int => Int)(a: Int, b: Int)Int
  
  sum(x => x)(1, 3)                               //> res0: Int = 6
  sum(x => x * x)(1, 3)                           //> res1: Int = 14
  sum(x => x * x * x)(1, 3)                       //> res2: Int = 36
}