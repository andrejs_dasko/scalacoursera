package week2

object rationals {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val r = new Rational(1, 2)                      //> r  : week2.Rational = 1/2
  r.numer                                         //> res0: Int = 1
  r.denom                                         //> res1: Int = 2
  val o = new Rational(1, 4)                      //> o  : week2.Rational = 1/4
  -o                                              //> res2: week2.Rational = 1/-4
  
  r + o                                           //> res3: week2.Rational = 3/4
  r - o                                           //> res4: week2.Rational = 1/4
  val x = new Rational(1, 3)                      //> x  : week2.Rational = 1/3
  val y = new Rational(5, 7)                      //> y  : week2.Rational = 5/7
  val z = new Rational(3, 2)                      //> z  : week2.Rational = 3/2
  
  x + y                                           //> res5: week2.Rational = 22/21
  x - y - z                                       //> res6: week2.Rational = -79/42
  new Rational(10, 20)                            //> res7: week2.Rational = 1/2
  x < y                                           //> res8: Boolean = true
  y < y                                           //> res9: Boolean = false
  x max y                                         //> res10: week2.Rational = 5/7
  y max x                                         //> res11: week2.Rational = 5/7
  x max x                                         //> res12: week2.Rational = 1/3
  x - x                                           //> res13: week2.Rational = 0/1
}

class Rational(x: Int, y: Int) {
	require(y != 0, "Denominator must be non-zero")

	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
	private val g = gcd(x, y)
	val numer = x / g
	val denom = y / g
	
	def + (r: Rational) = new Rational(r.numer * denom + numer * r.denom, r.denom * denom)
	
	def unary_- : Rational = new Rational(-numer, denom)
	
	def - (that: Rational) = this + -that // same as this.add(r.neg)
	
	def < (that: Rational): Boolean = numer * that.denom < that.numer * denom
	
	def max(that: Rational): Rational = if (this < that) that else this
	
	override def toString = numer + "/" + denom
}