def intToInt(x: Int) = x * 2
def intToList(x: Int) = List(x, x, x)

val list = List(1, 2, 3)
val listOfLists = list.map(intToList)

// list -> list of lists -> list
// list.flatMap(f) = list.map(f).flatten
list.flatMap(intToList)
list.map(intToList).flatten
listOfLists.flatMap(collection => for(elem <- collection) yield elem)
listOfLists.flatten
for (list <- listOfLists; elem <- list) yield elem