package week5

object moreListsWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def init[T](xs: List[T]): List[T] = xs match {
	  case List() => throw new Error("init of empty list")
	  case List(x) => List()
	  case y :: ys => y :: init(ys)
	  
	}                                         //> init: [T](xs: List[T])List[T]
	
	val list = List(1,2,3,4,5,6,7)            //> list  : List[Int] = List(1, 2, 3, 4, 5, 6, 7)
	init(list)                                //> res0: List[Int] = List(1, 2, 3, 4, 5, 6)
	init(List(1))                             //> res1: List[Int] = List()
	
	def concat[T](xs: List[T], ys: List[T]): List[T] = xs match {
		case List() => ys
		case z :: zs => z :: concat(zs, ys)
	}                                         //> concat: [T](xs: List[T], ys: List[T])List[T]
	
	val list2 = List(8,9,10,11,12)            //> list2  : List[Int] = List(8, 9, 10, 11, 12)
	
	concat(list, list2)                       //> res2: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
	
  def reverse[T](xs: List[T]): List[T] = xs match {
		case List() => xs
		case z :: zs => reverse(zs) ++ List(z)
	}                                         //> reverse: [T](xs: List[T])List[T]
	reverse(list)                             //> res3: List[Int] = List(7, 6, 5, 4, 3, 2, 1)
	
	def removeAt[T](n: Int, xs: List[T]): List[T] = (xs take n) ::: (xs.drop(n+1))
                                                  //> removeAt: [T](n: Int, xs: List[T])List[T]
	
	removeAt(3, List('a', 'b', 'c', 'd'))     //> res4: List[Char] = List(a, b, c)
	
	// TODO: implement mergesort
	def msort(xs: List[Int]): List[Int] = {
		val n = xs.length / 2
	  if (n == 0) xs
	  else {
			def merge(xs: List[Int], ys: List[Int]): List[Int] = (xs, ys) match {
				case (Nil, ys) => ys
				case (xs, Nil) => xs
				case (x :: xs1, y :: ys1) => {
					if (x < y) x :: merge(xs1, ys)
					else y :: merge(xs, ys1)
				}
			}
			val (fst, snd) = xs splitAt n
			merge(msort(fst), msort(snd))
		}
	}                                         //> msort: (xs: List[Int])List[Int]
	
	val unsorted = List(-4 ,2, 7, 5, 1)       //> unsorted  : List[Int] = List(-4, 2, 7, 5, 1)
	msort(unsorted)                           //> res5: List[Int] = List(-4, 1, 2, 5, 7)
}