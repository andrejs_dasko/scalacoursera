package week5

object workFoldExercisesWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  // Exercise 2
  def sum(x: List[Int]): Int =
  	(x foldLeft 0)(_ + _)                     //> sum: (x: List[Int])Int

 
  // Exercise 3
  def length[A](x: List[A]): Int =
  	(x foldLeft 0)((acc, x) => acc + 1)       //> length: [A](x: List[A])Int

 
  // Exercise 4
  def map[A, B](x: List[A], f: A => B): List[B] =
  	(x foldRight List[B]())((x, acc) => f(x) :: acc)
                                                  //> map: [A, B](x: List[A], f: A => B)List[B]

 
  // Exercise 5
  def filter[A](x: List[A], p: A => Boolean): List[A] =
  	(x foldRight List[A]())((x, acc) => if (p(x)) x :: acc else acc)
                                                  //> filter: [A](x: List[A], p: A => Boolean)List[A]

 
  // Exercise 6
  def append[A](x: List[A], y: List[A]): List[A] =
  	(x foldRight y) ((x, acc) => x :: acc)    //> append: [A](x: List[A], y: List[A])List[A]

 
  // Exercise 7
  def concat[A](x: List[List[A]]): List[A] =
  	(x foldRight List[A]())((x , acc) => x ::: acc)
                                                  //> concat: [A](x: List[List[A]])List[A]

 
  // Exercise 8
  def concatMap[A, B](x: List[A], f: A => List[B]): List[B] =
  	(x foldRight List[B]())((x, acc) => f(x) ::: acc)
                                                  //> concatMap: [A, B](x: List[A], f: A => List[B])List[B]

  // Exercise 9
  /** raise error if the list is empty */
  def maximum(x: List[Int]): Int =
  x reduceLeft ((acc, x) => if (acc > x) acc else x)
                                                  //> maximum: (x: List[Int])Int

 
  // Exercise 10
  def reverse[A](x: List[A]): List[A] =
  	(x foldLeft List[A]())((acc, x) => x :: acc)
                                                  //> reverse: [A](x: List[A])List[A]
  
  // list  foldLeft acc ( (acc, x) => acc op x )
  // (((0 + x1) + x2) + x3)
  // list foldRight acc ( (x, acc) => x op acc )
  // (x1 + (x2 + (x3 + 0)))
  
  val list1 = List(1, 2, 3, 4)                    //> list1  : List[Int] = List(1, 2, 3, 4)
  val list2 = List(5, 6, 7, 8)                    //> list2  : List[Int] = List(5, 6, 7, 8)
  
  sum(list1)                                      //> res0: Int = 10
  length(list1)                                   //> res1: Int = 4
  map(list2, (x: Int) => x * 2)                   //> res2: List[Int] = List(10, 12, 14, 16)
  filter(list2, (x: Int) => x % 7 != 0)           //> res3: List[Int] = List(5, 6, 8)
  append(list1, list2)                            //> res4: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
  append(list2, list1)                            //> res5: List[Int] = List(5, 6, 7, 8, 1, 2, 3, 4)
  concat(List(list1, List('a', 'b', 'c'), list2)) //> res6: List[AnyVal{def getClass(): Class[_ >: Char with Int <: AnyVal]}] = L
                                                  //| ist(1, 2, 3, 4, a, b, c, 5, 6, 7, 8)
  concatMap(list1, (x: Int) => List(x, x+1, x+2)) //> res7: List[Int] = List(1, 2, 3, 2, 3, 4, 3, 4, 5, 4, 5, 6)
  maximum(List(7, 7, 7))                          //> res8: Int = 7
  reverse(append(reverse(list2),reverse(list1)))  //> res9: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
}