package week5

import scala.math.Ordering

object paramMergeSortWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def msort[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
  
		def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
			case (Nil, ys) => ys
			case (xs, Nil) => xs
			case (x :: xs1, y :: ys1) => {
				if (ord.lt(x,y)) x :: merge(xs1, ys)
				else y :: merge(xs, ys1)
			}
		}
		
		val n = xs.length / 2
	  if (n == 0) xs
	  else {
			val (fst, snd) = xs splitAt n
			merge(msort(fst), msort(snd))
		}
	}                                         //> msort: [T](xs: List[T])(implicit ord: scala.math.Ordering[T])List[T]
	val unsorted = List(-4 ,2, 7, 5, 1)       //> unsorted  : List[Int] = List(-4, 2, 7, 5, 1)
	val fruit = List("apple", "pineapple", "orange", "banana")
                                                  //> fruit  : List[String] = List(apple, pineapple, orange, banana)
	
	msort(unsorted)                           //> res0: List[Int] = List(-4, 1, 2, 5, 7)
	msort(fruit)                              //> res1: List[String] = List(apple, banana, orange, pineapple)
}