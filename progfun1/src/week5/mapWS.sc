package week5

object mapWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def squareList(xs: List[Int]): List[Int] = xs match {
    case Nil => xs
    case y :: ys => y * y :: squareList(ys)
  }                                               //> squareList: (xs: List[Int])List[Int]

	def squareList2(xs: List[Int]): List[Int] = xs map (y => y * y)
                                                  //> squareList2: (xs: List[Int])List[Int]
	
	val list = List(1, 2, 3, 4)               //> list  : List[Int] = List(1, 2, 3, 4)
	
	squareList(list)                          //> res0: List[Int] = List(1, 4, 9, 16)
	squareList2(list)                         //> res1: List[Int] = List(1, 4, 9, 16)
	
	val unsorted = List(-4 , 0, 2, 7, 5, 1)   //> unsorted  : List[Int] = List(-4, 0, 2, 7, 5, 1)
	val fruit = List("apple", "pineapple", "orange", "banana")
                                                  //> fruit  : List[String] = List(apple, pineapple, orange, banana)
	
	unsorted filter (x => x > 0)              //> res2: List[Int] = List(2, 7, 5, 1)
	unsorted filterNot (x => x > 0)           //> res3: List[Int] = List(-4, 0)
	unsorted partition (x => x > 0)           //> res4: (List[Int], List[Int]) = (List(2, 7, 5, 1),List(-4, 0))
	unsorted takeWhile (x => x != 7)          //> res5: List[Int] = List(-4, 0, 2)
	unsorted dropWhile (x => x != 7)          //> res6: List[Int] = List(7, 5, 1)
	unsorted span (x => x != 7)               //> res7: (List[Int], List[Int]) = (List(-4, 0, 2),List(7, 5, 1))

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil      => Nil
    case x :: xs1 => (xs takeWhile (y => y == x)) :: pack(xs1 dropWhile (y => y == x))
  }                                               //> pack: [T](xs: List[T])List[List[T]]
  
  def pack2[T](xs: List[T]): List[List[T]] = xs match {
    case Nil      => Nil
    case x :: xs1 => {
    	val (first, rest) = xs span (y => y == x)
    	first :: pack2(rest)
    }
  }                                               //> pack2: [T](xs: List[T])List[List[T]]
  
  val testList = List("a", "a", "a", "b", "c", "c", "a")
                                                  //> testList  : List[String] = List(a, a, a, b, c, c, a)
  pack(testList)                                  //> res8: List[List[String]] = List(List(a, a, a), List(b), List(c, c), List(a)
                                                  //| )
	pack2(testList)                           //> res9: List[List[String]] = List(List(a, a, a), List(b), List(c, c), List(a)
                                                  //| )
  def encode[T](xs: List[T]): List[(T, Int)] =
  	pack(xs) map (ys => (ys.head, ys.length)) //> encode: [T](xs: List[T])List[(T, Int)]
  
  encode(testList)                                //> res10: List[(String, Int)] = List((a,3), (b,1), (c,2), (a,1))
}