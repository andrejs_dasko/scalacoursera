package week3

import week2.Rational
// import week2._
// import week2.{Rational, Hello}

object classHierarchiesWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val x = new NonEmpty(5, Empty, Empty)           //> x  : week3.NonEmpty = {.5.}
  val y = x incl 4                                //> y  : week3.IntSet = {{.4.}5.}
  val z = new NonEmpty(7, Empty, Empty)           //> z  : week3.NonEmpty = {.7.}
  y union z                                       //> res0: week3.IntSet = {{.4{.5.}}7.}
  new Rational(1, 2)                              //> res1: week2.Rational = 1/2
  
  if (true) 1 else false                          //> res2: AnyVal = 1
  val n = null                                    //> n  : Null = null
}