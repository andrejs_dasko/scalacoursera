package week3

object polymorphismWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def nth[T](n: Int, list: List[T]): T = {
  	if (n < 0 || list.isEmpty) throw new IndexOutOfBoundsException
  	else if (n == 0) list.head
  	else nth(n - 1, list.tail)
  }                                               //> nth: [T](n: Int, list: week3.List[T])T
  
  def singleton[T](elem: T): List[T] = new Cons[T](elem, new Nil)
                                                  //> singleton: [T](elem: T)week3.List[T]
  singleton(1)                                    //> res0: week3.List[Int] = 1 -> .
  val x = new Cons[Int](1, new Cons(3, new Cons(2, new Cons(7, new Nil))))
                                                  //> x  : week3.Cons[Int] = 1 -> 3 -> 2 -> 7 -> .
  nth(2, x)                                       //> res1: Int = 2
}