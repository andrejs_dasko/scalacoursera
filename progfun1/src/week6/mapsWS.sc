package week6

object mapsWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val fruit = List("apple", "pear", "orange", "pineapple")
                                                  //> fruit  : List[String] = List(apple, pear, orange, pineapple)
  
  fruit groupBy (_.head)                          //> res0: scala.collection.immutable.Map[Char,List[String]] = Map(p -> List(pear
                                                  //| , pineapple), a -> List(apple), o -> List(orange))
  val map = fruit groupBy (_.length)              //> map  : scala.collection.immutable.Map[Int,List[String]] = Map(5 -> List(appl
                                                  //| e), 4 -> List(pear), 9 -> List(pineapple), 6 -> List(orange))
  map.toSeq.sortBy(_._1)                          //> res1: Seq[(Int, List[String])] = ArrayBuffer((4,List(pear)), (5,List(apple))
                                                  //| , (6,List(orange)), (9,List(pineapple)))
                                                  
  class Poly(terms0: Map[Int, Double]) {
  	def this(bindings: (Int, Double)*) = this(bindings.toMap)
  	
  	val terms = terms0 withDefaultValue(0.0)
  	def + (other: Poly) = new Poly(terms ++ (other.terms map adjust))
  	def plus (other: Poly) = {
  		// new Poly((other.terms foldLeft terms)((acc, term) => acc + (term._1 -> (acc(term._1) + term._2))))
  		new Poly((other.terms foldLeft terms)(addTerm))
  	}
  	def addTerm(terms: Map[Int, Double], term: (Int, Double)): Map[Int, Double] = {
  		val (exp, coef) = term
  		terms + (exp -> (terms(exp) + coef))
  	}
  	def adjust(term: (Int, Double)): (Int, Double) = {
  		val (exp, coeff) = term
  		exp -> (terms(exp) + coeff)
  	}
  	
  	override def toString = (for ((exp, coef) <- terms.toList.sorted.reverse) yield coef + "x^" + exp) mkString " + "
  }
   
  val p1 = new Poly(1 -> 2.0, 3 -> 4.0, 5 -> 6.2) //> p1  : week6.mapsWS.Poly = 6.2x^5 + 4.0x^3 + 2.0x^1
  val p2 = new Poly(Map(0 -> 3.0, 3 -> 7.0))      //> p2  : week6.mapsWS.Poly = 7.0x^3 + 3.0x^0
  p1 + p2                                         //> res2: week6.mapsWS.Poly = 6.2x^5 + 11.0x^3 + 2.0x^1 + 3.0x^0
  p1 plus p2                                      //> res3: week6.mapsWS.Poly = 6.2x^5 + 11.0x^3 + 2.0x^1 + 3.0x^0
}