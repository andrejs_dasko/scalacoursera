package week6

object StringParserTerrainWS {
  case class Pos(x: Int, y: Int) {
    /** The position obtained by changing the `x` coordinate by `d` */
    def dx(d: Int) = copy(x = x + d)

    /** The position obtained by changing the `y` coordinate by `d` */
    def dy(d: Int) = copy(y = y + d)
  }

  def terrainFunction(levelVector: Vector[Vector[Char]]): Pos => Boolean = {
    (p: Pos) => p.x < levelVector.length && p.y < levelVector(p.x).length
  }                                               //> terrainFunction: (levelVector: Vector[Vector[Char]])week6.StringParserTerrai
                                                  //| nWS.Pos => Boolean
  
  def findChar(c: Char, levelVector: Vector[Vector[Char]]): Pos = {
    //    val x = (for (row <- levelVector; if row.contains(c)) yield Pos(levelVector.indexOf(row), row.indexOf(c)))
    //    x(1)
    val col = levelVector.indexWhere(row => row contains c)
    Pos(col, levelVector(col).indexOf(c))
  }                                               //> findChar: (c: Char, levelVector: Vector[Vector[Char]])week6.StringParserTerr
                                                  //| ainWS.Pos
  val test = Vector(Vector('S', 'T'), Vector('o', 'o'), Vector('o', 'o'))
                                                  //> test  : scala.collection.immutable.Vector[scala.collection.immutable.Vector[
                                                  //| Char]] = Vector(Vector(S, T), Vector(o, o), Vector(o, o))
  
  def map = terrainFunction(test)                 //> map: => week6.StringParserTerrainWS.Pos => Boolean
  map(Pos(0,0))                                   //> res0: Boolean = true
  map(Pos(0,1))                                   //> res1: Boolean = true
  map(Pos(3,0))                                   //> res2: Boolean = false
  map(Pos(0,2))                                   //> res3: Boolean = false
  
  findChar('S', test)                             //> res4: week6.StringParserTerrainWS.Pos = Pos(0,0)
  findChar('T', test)                             //> res5: week6.StringParserTerrainWS.Pos = Pos(0,1)
  findChar('o', test)                             //> res6: week6.StringParserTerrainWS.Pos = Pos(1,0)
}