package week6

object SequencesWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  val s = "Hello world"                           //> s  : String = Hello world
  val pairs = (1 to s.length) zip s               //> pairs  : scala.collection.immutable.IndexedSeq[(Int, Char)] = Vector((1,H), 
                                                  //| (2,e), (3,l), (4,l), (5,o), (6, ), (7,w), (8,o), (9,r), (10,l), (11,d))
  val (first, last) = pairs.unzip                 //> first  : scala.collection.immutable.IndexedSeq[Int] = Vector(1, 2, 3, 4, 5, 
                                                  //| 6, 7, 8, 9, 10, 11)
                                                  //| last  : scala.collection.immutable.IndexedSeq[Char] = Vector(H, e, l, l, o, 
                                                  //|  , w, o, r, l, d)
  1 to 10 flatMap (x => (1 to 10 map (y => (x, y))))
                                                  //> res0: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((1,1), (1,2
                                                  //| ), (1,3), (1,4), (1,5), (1,6), (1,7), (1,8), (1,9), (1,10), (2,1), (2,2), (2
                                                  //| ,3), (2,4), (2,5), (2,6), (2,7), (2,8), (2,9), (2,10), (3,1), (3,2), (3,3), 
                                                  //| (3,4), (3,5), (3,6), (3,7), (3,8), (3,9), (3,10), (4,1), (4,2), (4,3), (4,4)
                                                  //| , (4,5), (4,6), (4,7), (4,8), (4,9), (4,10), (5,1), (5,2), (5,3), (5,4), (5,
                                                  //| 5), (5,6), (5,7), (5,8), (5,9), (5,10), (6,1), (6,2), (6,3), (6,4), (6,5), (
                                                  //| 6,6), (6,7), (6,8), (6,9), (6,10), (7,1), (7,2), (7,3), (7,4), (7,5), (7,6),
                                                  //|  (7,7), (7,8), (7,9), (7,10), (8,1), (8,2), (8,3), (8,4), (8,5), (8,6), (8,7
                                                  //| ), (8,8), (8,9), (8,10), (9,1), (9,2), (9,3), (9,4), (9,5), (9,6), (9,7), (9
                                                  //| ,8), (9,9), (9,10), (10,1), (10,2), (10,3), (10,4), (10,5), (10,6), (10,7), 
                                                  //| (10,8), (10,9), (10,10))
  val list1 = List(1, 2, 3)                       //> list1  : List[Int] = List(1, 2, 3)
  val list2 = List(1, 2, 3)                       //> list2  : List[Int] = List(1, 2, 3)

  (list1 zip list2).map(xy => xy._1 * xy._2).sum  //> res1: Int = 14
  (list1 zip list2).map { case (x, y) => x * y }.sum
                                                  //> res2: Int = 14

  def isPrime(n: Int): Boolean = (2 until n) forall (x => n % x != 0)
                                                  //> isPrime: (n: Int)Boolean
  isPrime(1)                                      //> res3: Boolean = true
  isPrime(2)                                      //> res4: Boolean = true
  isPrime(3)                                      //> res5: Boolean = true
  isPrime(4)                                      //> res6: Boolean = false
  isPrime(5)                                      //> res7: Boolean = true
  isPrime(6)                                      //> res8: Boolean = false
  isPrime(7)                                      //> res9: Boolean = true
  isPrime(8)                                      //> res10: Boolean = false
  isPrime(9)                                      //> res11: Boolean = false
  isPrime(10)                                     //> res12: Boolean = false
  isPrime(11)                                     //> res13: Boolean = true
  isPrime(12)                                     //> res14: Boolean = false
  isPrime(13)                                     //> res15: Boolean = true
  isPrime(14)                                     //> res16: Boolean = false
  isPrime(15)                                     //> res17: Boolean = false
  isPrime(16)                                     //> res18: Boolean = false
  isPrime(17)                                     //> res19: Boolean = true
  isPrime(18)                                     //> res20: Boolean = false
  isPrime(19)                                     //> res21: Boolean = true
}