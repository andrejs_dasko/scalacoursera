package week6

object queensWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def queens(n: Int): Set[List[Int]] = {
  	def isSafe(col: Int, queens: List[Int]): Boolean = {
  		val newQueenRow = queens.length
    val matrix = for {
      r <- queens.length - 1 to 0 by -1
    } yield (r, queens(r))
  		false
  	}
  	def placeQueens(k: Int): Set[List[Int]] = {
  		if (k == 0) Set(List())
  		else
  			for {
  				queens <- placeQueens(k - 1)
  				col <- 0 until n
  				if isSafe(col, queens)
  			} yield col :: queens
  	}
  	placeQueens(n)
  }                                               //> queens: (n: Int)Set[List[Int]]

  def isSafe(col: Int, queens: List[Int]): IndexedSeq[(Int, Int)] = {
    val newQueenRow = queens.length
    val matrix = for {
      r <- queens.length - 1 to 0 by -1
    } yield (r, queens(r))
    matrix
  }                                               //> isSafe: (col: Int, queens: List[Int])IndexedSeq[(Int, Int)]
  
  val queenss = List(1, 3, 0)                     //> queenss  : List[Int] = List(1, 3, 0)
  isSafe(3, queenss)                              //> res0: IndexedSeq[(Int, Int)] = Vector((2,0), (1,3), (0,1))
  (queenss.length - 1 to 0 by -1) zip queenss.reverse
                                                  //> res1: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,0), (1,3
                                                  //| ), (0,1))
}