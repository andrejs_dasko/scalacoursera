package week6

object forWS {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  def isPrime(n: Int): Boolean = (2 until n) forall (x => n % x != 0)
                                                  //> isPrime: (n: Int)Boolean
    
  val n = 7                                       //> n  : Int = 7
  val pairs = (1 until n) map (i => (1 until i) map (j => (i, j)))
                                                  //> pairs  : scala.collection.immutable.IndexedSeq[scala.collection.immutable.In
                                                  //| dexedSeq[(Int, Int)]] = Vector(Vector(), Vector((2,1)), Vector((3,1), (3,2))
                                                  //| , Vector((4,1), (4,2), (4,3)), Vector((5,1), (5,2), (5,3), (5,4)), Vector((6
                                                  //| ,1), (6,2), (6,3), (6,4), (6,5)))
  val flattenedPairs = pairs.flatten              //> flattenedPairs  : scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector
                                                  //| ((2,1), (3,1), (3,2), (4,1), (4,2), (4,3), (5,1), (5,2), (5,3), (5,4), (6,1)
                                                  //| , (6,2), (6,3), (6,4), (6,5))
  val primePairs = flattenedPairs filter ( xy => isPrime(xy._1 + xy._2) )
                                                  //> primePairs  : scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,
                                                  //| 1), (3,2), (4,1), (4,3), (5,2), (6,1), (6,5))
  val primes = primePairs map {case (x, y) => x + y}
                                                  //> primes  : scala.collection.immutable.IndexedSeq[Int] = Vector(3, 5, 5, 7, 7,
                                                  //|  7, 11)
	primes.distinct                           //> res0: scala.collection.immutable.IndexedSeq[Int] = Vector(3, 5, 7, 11)
	
	
	(pairs foldLeft (Vector[(Int, Int)]())) ((acc, v) => acc ++ v)
                                                  //> res1: scala.collection.immutable.Vector[(Int, Int)] = Vector((2,1), (3,1), (
                                                  //| 3,2), (4,1), (4,2), (4,3), (5,1), (5,2), (5,3), (5,4), (6,1), (6,2), (6,3), 
                                                  //| (6,4), (6,5))
	(pairs foldRight (Vector[(Int, Int)]())) ((v, acc) => acc ++ v)
                                                  //> res2: scala.collection.immutable.Vector[(Int, Int)] = Vector((6,1), (6,2), (
                                                  //| 6,3), (6,4), (6,5), (5,1), (5,2), (5,3), (5,4), (4,1), (4,2), (4,3), (3,1), 
                                                  //| (3,2), (2,1))
	for {
		i <- (1 until n)
		j <- (1 until i)
		if isPrime(i + j)
		if (i + j) > 5
	} yield i + j                             //> res3: scala.collection.immutable.IndexedSeq[Int] = Vector(7, 7, 7, 11)
	
	def scalarProduct(xs: List[Double], ys: List[Double]): Double =
		(for ( (x, y) <- (xs zip ys) ) yield x * y).sum
                                                  //> scalarProduct: (xs: List[Double], ys: List[Double])Double
		
	val list1 = List(1.0, 2.0, 3.0, 4.0)      //> list1  : List[Double] = List(1.0, 2.0, 3.0, 4.0)
	scalarProduct(list1, list1)               //> res4: Double = 30.0
}