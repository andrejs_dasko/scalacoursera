package week6

object assignment6WS {

  /** A word is simply a `String`. */
  type Word = String

  /** A sentence is a `List` of words. */
  type Sentence = List[Word]

  /**
   * `Occurrences` is a `List` of pairs of characters and positive integers saying
   *  how often the character appears.
   *  This list is sorted alphabetically w.r.t. to the character in each pair.
   *  All characters in the occurrence list are lowercase.
   *
   *  Any list of pairs of lowercase characters and their frequency which is not sorted
   *  is **not** an occurrence list.
   *
   *  Note: If the frequency of some character is zero, then that character should not be
   *  in the list.
   */
  type Occurrences = List[(Char, Int)]

  def wordOccurrences(w: String): List[(Char, Int)] = {
    // Map(a -> aa, b -> bbb)
    val grouped = w groupBy ((c: Char) => c)
    // Map(a -> 2, b -> 3)
    val resultMap = grouped map { case (k, v) => (k, v.length) }
    resultMap.toList.sorted
  }                                               //> wordOccurrences: (w: String)List[(Char, Int)]

  val word = "abbcccdddd"                         //> word  : String = abbcccdddd
  wordOccurrences(word)                           //> res0: List[(Char, Int)] = List((a,1), (b,2), (c,3), (d,4))

  // List(('a', 2), ('b', 2))
  def combinations(occurrences: List[(Char, Int)]): List[List[(Char, Int)]] = {
    if (occurrences.isEmpty) List(List())
    else {
      for {
        (ltr, n) <- occurrences
        t <- n to 1 by -1
      } yield List((ltr, t))
    }
  }                                               //> combinations: (occurrences: List[(Char, Int)])List[List[(Char, Int)]]

  combinations(List(('a', 2), ('b', 2))).flatten  //> res1: List[(Char, Int)] = List((a,2), (a,1), (b,2), (b,1))

  val list = List(1, 2, 3)                        //> list  : List[Int] = List(1, 2, 3)
  list.permutations.toList                        //> res2: List[List[Int]] = List(List(1, 2, 3), List(1, 3, 2), List(2, 1, 3), L
                                                  //| ist(2, 3, 1), List(3, 1, 2), List(3, 2, 1))
  def remove[T](n: T, list: List[T]) = list diff List(n)
                                                  //> remove: [T](n: T, list: List[T])List[T]

  list.combinations(2).toList                     //> res3: List[List[Int]] = List(List(1, 2), List(1, 3), List(2, 3))
  for {
    n <- list
  } yield remove(n, list).permutations.toList     //> res4: List[List[List[Int]]] = List(List(List(2, 3), List(3, 2)), List(List(
                                                  //| 1, 3), List(3, 1)), List(List(1, 2), List(2, 1)))

  def perm(list: List[Int]): List[List[Int]] = {
    val selfPermutations = list.permutations.toList
    selfPermutations ::: (list flatMap (x => perm(remove(x, list))))
  }                                               //> perm: (list: List[Int])List[List[Int]]

  perm(list).distinct                             //> res5: List[List[Int]] = List(List(1, 2, 3), List(1, 3, 2), List(2, 1, 3), L
                                                  //| ist(2, 3, 1), List(3, 1, 2), List(3, 2, 1), List(2, 3), List(3, 2), List(3)
                                                  //| , List(), List(2), List(1, 3), List(3, 1), List(1), List(1, 2), List(2, 1))
                                                  //| 

  val y = list foreach (x => println(remove(x, list).permutations.toList))
                                                  //> List(List(2, 3), List(3, 2))
                                                  //| List(List(1, 3), List(3, 1))
                                                  //| List(List(1, 2), List(2, 1))
                                                  //| y  : Unit = ()

  val fM = list flatMap (x => remove(x, list).permutations.toList)
                                                  //> fM  : List[List[Int]] = List(List(2, 3), List(3, 2), List(1, 3), List(3, 1)
                                                  //| , List(1, 2), List(2, 1))

  def combinations2(occurrences: List[(Char, Int)]) = {
    // step 1
    val expanded = occurrences map { case (ltr, num) => (for (i <- 1 to num) yield (ltr, i)).toList }
    // step 2
    def loop(exp: List[List[(Char, Int)]], acc: List[List[(Char, Int)]]): List[List[(Char, Int)]] = exp match {
      case List() => acc
      case _ => {
        val head = for (pair <- exp.head) yield List(pair)
        val combinations = for {
          p2 <- acc
          p1 <- exp.head
        } yield p1 :: p2
        loop(exp.tail, acc ++ head ++ combinations)
      }
    }
    loop(expanded, List(List()))
  }                                               //> combinations2: (occurrences: List[(Char, Int)])List[List[(Char, Int)]]
  combinations2(List(('a', 2), ('b', 2)))         //> res6: List[List[(Char, Int)]] = List(List(), List((a,1)), List((a,2)), List
                                                  //| ((a,1)), List((a,2)), List((b,1)), List((b,2)), List((b,1)), List((b,2)), L
                                                  //| ist((b,1), (a,1)), List((b,2), (a,1)), List((b,1), (a,2)), List((b,2), (a,2
                                                  //| )), List((b,1), (a,1)), List((b,2), (a,1)), List((b,1), (a,2)), List((b,2),
                                                  //|  (a,2)))

  val emptyList = List()                          //> emptyList  : List[Nothing] = List()
  val list2elems = List(1, 2)                     //> list2elems  : List[Int] = List(1, 2)

  for {
    fe <- list2elems
    ee <- emptyList
  } yield List(ee, fe)                            //> res7: List[List[Int]] = List()

  val lard = List(('a', 3), ('d', 1), ('l', 1), ('r', 1))
                                                  //> lard  : List[(Char, Int)] = List((a,3), (d,1), (l,1), (r,1))
  val r = List(('r', 1))                          //> r  : List[(Char, Int)] = List((r,1))
  var ar = List(('a', 1),('r', 1))                //> ar  : List[(Char, Int)] = List((a,1), (r,1))

  val map = lard.toMap                            //> map  : scala.collection.immutable.Map[Char,Int] = Map(a -> 3, d -> 1, l -> 
                                                  //| 1, r -> 1)
  map.get(r.head._1) match {
    case Some(num) => if (num - r.head._2 > 0) map.updated(r.head._1, num - r.head._2) else map - r.head._1
    case None      => map
  }                                               //> res8: scala.collection.immutable.Map[Char,Int] = Map(a -> 3, d -> 1, l -> 1
                                                  //| )
  def subtract(map: Occurrences, sub: Occurrences): Occurrences = {
    def subPair(acc: Map[Char, Int], pair: (Char, Int)): Map[Char, Int] = {
    	val map = acc.toMap
      map.get(pair._1) match {
        case Some(num) => if (num - pair._2 > 0) map.updated(pair._1, num - pair._2) else map - pair._1
        case None      => map
      }
    }
    ((sub.toMap foldLeft map.toMap)(subPair)).toList
  }                                               //> subtract: (map: week6.assignment6WS.Occurrences, sub: week6.assignment6WS.O
                                                  //| ccurrences)week6.assignment6WS.Occurrences
  subtract(lard, r)                               //> res9: week6.assignment6WS.Occurrences = List((a,3), (d,1), (l,1))
  subtract(lard, ar)                              //> res10: week6.assignment6WS.Occurrences = List((a,2), (d,1), (l,1))
}